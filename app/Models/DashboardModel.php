<?php

namespace App\Models;

use CodeIgniter\Model;

class DashboardModel extends Model
{
    protected $table = 'stock';
    protected $primaryKey = 'id_barang';
    protected $allowedFields = ['Nama_barang', 'thumb', 'desc', 'harga', 'stok', 'tipe'];
}
