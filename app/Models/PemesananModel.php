<?php

namespace App\Models;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class PemesananModel extends Model
{
    protected $table = 'pemesanan';
    protected $primaryKey = 'id_pemesanan';
    protected $allowedFields = ['id_users', 'id_barang', 'jumlah', 'total_harga','status', 'pembayaran','pengantaran','waktu', 'created_at','alamat','nohp'];

    

    function get_pemesanan(){
        
        $db      = \Config\Database::connect();
        return $this->db->table('pemesanan')
        ->join('users','users.id_users = pemesanan.id_users')
        ->join('stock','stock.id_barang = pemesanan.id_barang')
        ->get()->getResultArray();
    }
    function get_barang($id=null){
        
        $db      = \Config\Database::connect();

        $row = $this->db->table('pemesanan')
        ->join('users','users.id_users = pemesanan.id_users')
        ->join('stock','stock.id_barang = pemesanan.id_barang')
        ->where('users.id_users', $id)
        ->get();
        
    }
    
    function add_pemesanan($data){
        
        $db      = \Config\Database::connect();
        return $this->db->table('pemesanan')->insert($data);
        
    }
    
    public function getUser($id)
   {
      // Get the user with the specified ID
      return $this->db->table('pemesanan')->getWhere(['id_users' => $id])->getRow();
   }

    protected function beforeInsert(array $data){
    $data['data']['created_at'] = date('Y-m-d H:i:s');

    return $data;
  }
}

