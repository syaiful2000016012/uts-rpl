<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.6.2.js" integrity="sha256-pkn2CUZmheSeyssYw3vMp1+xyub4m+e+QK4sQskvuo4=" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="/assets/css/dashboarduser.css">
  <script src="/assets/js/dashboard.js"></script>
  <title></title>

</head>

<body>
  <?php
  $uri = service('uri');
  ?>
  <nav class="navbar navbar-expand-lg navbar">
    <div class="container">
      <a class="navbar-brand" href="/dashboardUser/<?= ($uri->getSegment(2))?>">Captain <span id="ciken">Chicken</span> </a>
      <button class="navbar-toggler first-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent20" aria-controls="navbarSupportedContent20" aria-expanded="false" aria-label="Toggle navigation" id="burger" onclick="bar()">
        <div class="animated-icon1"><span></span><span></span><span></span></div>
      </button>
      <div class="navbar-collapse" id="navbarr">
        <?php if (session()->get('isLoggedIn')) : ?>
          <ul class="navbar-nav mr-auto">
            <li class="nav-item <?= ($uri->getSegment(2) == 'dashboard' ? 'active' : null) ?>">
              <a class="nav-link" href="/dashboardUser/<?= ($uri->getSegment(2))?>">Home</a>
            </li>
            <li class="nav-item <?= ($uri->getSegment(2) == 'dashboard' ? 'active' : null) ?>">
              <a class="nav-link" href="/pesanan/<?= ($uri->getSegment(2))?>">Pesanan</a>
            </li>
            <li class="nav-item <?= ($uri->getSegment(2) == 'profile' ? 'active' : null) ?>">
              <a class="nav-link" href="/profile/<?= ($uri->getSegment(2))?>">Profile</a>
            </li>
          </ul>
          <ul class="navbar-nav my-2 my-lg-0">
            <li class="nav-item">
              <a class="nav-link" href="/logout">Logout</a>
            </li>
          </ul>
        <?php else : ?>
          <ul class="navbar-nav mr-auto">
            <li class="nav-item <?= ($uri->getSegment(2) == '' ? 'active' : null) ?>">
              <a class="nav-link" href="/">Login</a>
            </li>
            <li class="nav-item <?= ($uri->getSegment(2) == 'register' ? 'active' : null) ?>">
              <a class="nav-link" href="/register">Register</a>
            </li>
          </ul>
        <?php endif; ?>
      </div>
    </div>
  </nav>
  <div class="container">
    <div class="row">
      <div class="greeting">
        <h1>Hello, <span id="sub-greet"> <?= session()->get('firstname') ?> <?= session()->get('lastname') ?></span></h1>
      </div>
    </div>
    
  </div>
  <div class="con">
     <?php if (session()->getFlashdata('sukses')): ?>
    <div class="alert alert-success" style="font-size: 12px;">
        <?= session()->getFlashdata('sukses') ?>
    </div>
<?php endif ?>
    <?php
    foreach ($pemesan as $user =>$value) {
      // var_dump($user);
    ?>
    <div class="sub-con">
      <img src="/assets/image/ayam.png" alt="">
      <h3>Ayam Bersih</h3>
      <p>Ayam yang dipotong dan dibersihkan</p>
      <a href="<?= base_url('ayambersih/'.$value['id_users']) ?>"><button>Belanja!</button></a>
    </div>
    <div class="sub-con">
      <img src="/assets/image/ayam hidup.jpg" alt="">
      <h3>Ayam hidup</h3>
      <p>Ayam yang Belum dipotong dan belum dibersihkan</p>
      <a href="<?= base_url('ayamkotor/'.$value['id_users']) ?>"><button>Belanja!</button></a>
    </div>
  </div>
  <?php
    }

    ?>
    
      </div>
    </div>
  </div>