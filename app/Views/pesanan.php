<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
      crossorigin="anonymous"
    />
    <script
      src="https://code.jquery.com/jquery-3.6.2.js"
      integrity="sha256-pkn2CUZmheSeyssYw3vMp1+xyub4m+e+QK4sQskvuo4="
      crossorigin="anonymous"
    ></script>
    <script src="https://unpkg.com/scrollreveal"></script>
    <link rel="stylesheet" href="/assets/css/pemesanan.css" />

    <title>Document</title>
  </head>
  <style>
  #navbarr {
  display: none;
}
</style>
  <body>
    <?php
  $uri = service('uri');
  ?>
    <nav class="navbar navbar-expand-lg navbar">
    <div class="container">
      <a class="navbar-brand" href="/dashboardUser/<?= ($uri->getSegment(2))?>">Captain <span id="ciken">Chicken</span> </a>
      <button class="navbar-toggler first-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent20" aria-controls="navbarSupportedContent20" aria-expanded="false" aria-label="Toggle navigation" id="burger"  onclick="bar()">
        <div class="animated-icon1"><span></span><span></span><span></span></div>
      </button>
      <div class="navbar-collapse" id="navbarr">
        <?php if (session()->get('isLoggedIn')) : ?>
          <ul class="navbar-nav mr-auto">
            <li class="nav-item <?= ($uri->getSegment(2) == 'dashboardUser' ? 'active' : null) ?>">
              <a class="nav-link" href="/dashboardUser/<?= ($uri->getSegment(2))?>">Home</a>
            </li>
            <li class="nav-item <?= ($uri->getSegment(2) == 'pemesanan' ? 'active' : null) ?>">
              <a class="nav-link" href="/pesanan/<?= ($uri->getSegment(2))?>">Pesanan</a>
            </li>
            <li class="nav-item <?= ($uri->getSegment(2) == 'profile' ? 'active' : null) ?>">
              <a class="nav-link" href="/profile/<?= ($uri->getSegment(2))?>">Profile</a>
            </li>
          </ul>
          <ul class="navbar-nav my-2 my-lg-0">
            <li class="nav-item">
              <a class="nav-link" href="/logout">Logout</a>
            </li>
          </ul>
        <?php else : ?>
          <ul class="navbar-nav mr-auto">
            <li class="nav-item <?= ($uri->getSegment(2) == '' ? 'active' : null) ?>">
              <a class="nav-link" href="/">Login</a>
            </li>
            <li class="nav-item <?= ($uri->getSegment(2) == 'register' ? 'active' : null) ?>">
              <a class="nav-link" href="/register">Register</a>
            </li>
          </ul>
        <?php endif; ?>
      </div>
    </div>
  </nav>
  

    <div class="isi_brg">
      <?php if (session()->getFlashdata('succes')): ?>
    <div class="alert alert-success" style="font-size: 12px;">
        <?= session()->getFlashdata('succes') ?>
    </div>
<?php endif ?>
 <?php if (session()->getFlashdata('sukses')): ?>
    <div class="alert alert-success" style="font-size: 12px;">
        <?= session()->getFlashdata('sukses') ?>
    </div>
<?php endif ?>
      <?php
      $count =0;
    foreach ($bayar as $key =>
      $value) { {$count++; if ($count == 2) {
        break;}} ?>
      
      <div class="content_brg">
        <div class="header">
        <img src="/assets/image/Logo Morgan.png" alt="" width="100px">
        <h3>STRUCK PEMESANAN</h3>
        <small><?= $value['created_at']; ?></small>
        <h6> <span id="statusp">Status Pembayaran : </span> <span id="status"><?= $value['status_bayar']; ?></span> </h6>
        <h6> <span id="statuspesan">Status Pemesanan : </span> <span id="statuspesan"><?= $value['status_pesanan']; ?></span> </h6>
        <?php
    foreach ($pemesan as $key =>
      $value) { // var_dump($user); ?>
        <p id="username"> <span id="nama">Pelanggan : </span>
          <?= $value['firstname']; ?>
          <?= $value['lastname']; ?>
        </p>
        <?php
    }

    ?>
      </div>
      <span id="batas">==========================</span>
      <?php $total = 0; ?>
      <?php
      $id = $uri->getSegment(2);
    foreach ($pemesanan as $key =>
      $value) { // var_dump($user); ?>
      <?php if ($value['id_users'] != $id): ?>
        <?php continue; ?>
    <?php endif ?>
      <div class="content">
        <p id="nama_barang"> <?= $value['Nama_barang']; ?></p>
        <div class="sub-content">
        <p id="harga">  <?= number_to_currency($value['harga'], 'IDR'); ?></p>
        <p id="jumlah"> <span id="jumlah">x </span> <?= $value['jumlah']; ?></p>
        <p id="total"><?= number_to_currency($value['harga'] * $value['jumlah'], 'IDR'); ?></p>
        
        <?php $total += $value['harga'] * $value['jumlah'];?>
        </div>
        </div>
        <?php
    }

    ?>
        <span id="batas">==========================</span>
        <div class="footer">
        <p id="total"> <span id="totall">Total Harga : </span> 
         <?= number_to_currency($total, 'IDR');  ?>
        </p>
      </div>
      </div>
      <?php
    }

    ?>


    <script>
      var pembayaran = document.getElementById('status').innerHTML;
      var lunas = 'lunas';
      var diproses = 'diproses';
      
      if (pembayaran == 'lunas') {
        document.getElementById('status').style.color='green';
      }
      else if(pembayaran == 'diproses'){
        document.getElementById('status').style.color='oranye';
      }
      else{
        document.getElementById('status').style.color='red';
      }
      console.log(pembayaran);
    </script>
   
    <script>
      $(document).ready(function () {
  $(".first-button").on("click", function () {
    $(".animated-icon1").toggleClass("open");
  });
});

function bar() {
  var x = document.getElementById("navbarr");
  if (x.style.display == "grid") {
    x.style.display = "none";
  } else {
    x.style.display = "grid";
  }
}
    </script>
  </body>
</html>
 