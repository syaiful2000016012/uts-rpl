<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
      crossorigin="anonymous"
    />
    <script
      src="https://code.jquery.com/jquery-3.6.2.js"
      integrity="sha256-pkn2CUZmheSeyssYw3vMp1+xyub4m+e+QK4sQskvuo4="
      crossorigin="anonymous"
    ></script>
    <script src="https://unpkg.com/scrollreveal"></script>
    <link rel="stylesheet" href="/assets/css/pemesanan.css" />

    <title>Document</title>
  </head>
  <style>
  #navbarr {
  display: none;
}
</style>
  <body>
    <?php
  $uri = service('uri');
  ?>
  
    <nav class="navbar navbar-expand-lg navbar">
    <div class="container">
      <a class="navbar-brand" href="/dashboardUser/<?= ($uri->getSegment(2))?>">Captain <span id="ciken">Chicken</span> </a>
      <button class="navbar-toggler first-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent20" aria-controls="navbarSupportedContent20" aria-expanded="false" aria-label="Toggle navigation" id="burger"  onclick="bar()">
        <div class="animated-icon1"><span></span><span></span><span></span></div>
      </button>
      <div class="navbar-collapse" id="navbarr">
        <?php if (session()->get('isLoggedIn')) : ?>
          <ul class="navbar-nav mr-auto">
            <li class="nav-item <?= ($uri->getSegment(2) == 'dashboardUser' ? 'active' : null) ?>">
              <a class="nav-link" href="/dashboardUser/<?= ($uri->getSegment(2))?>">Home</a>
            </li>
            <li class="nav-item <?= ($uri->getSegment(2) == 'pemesanan' ? 'active' : null) ?>">
              <a class="nav-link" href="/pesanan/<?= ($uri->getSegment(2))?>">Pesanan</a>
            </li>
            <li class="nav-item <?= ($uri->getSegment(2) == 'profile' ? 'active' : null) ?>">
              <a class="nav-link" href="/profile/<?= ($uri->getSegment(2))?>">Profile</a>
            </li>
          </ul>
          <ul class="navbar-nav my-2 my-lg-0">
            <li class="nav-item">
              <a class="nav-link" href="/logout">Logout</a>
            </li>
          </ul>
        <?php else : ?>
          <ul class="navbar-nav mr-auto">
            <li class="nav-item <?= ($uri->getSegment(2) == '' ? 'active' : null) ?>">
              <a class="nav-link" href="/">Login</a>
            </li>
            <li class="nav-item <?= ($uri->getSegment(2) == 'register' ? 'active' : null) ?>">
              <a class="nav-link" href="/register">Register</a>
            </li>
          </ul>
        <?php endif; ?>
      </div>
    </div>
  </nav>
  

    <div class="isi_brg">
      <?php if (session()->getFlashdata('succes')): ?>
    <div class="alert alert-success" style="font-size: 12px;">
        <?= session()->getFlashdata('succes') ?>
    </div>
<?php endif ?>
 <?php if (session()->getFlashdata('sukses')): ?>
    <div class="alert alert-success" style="font-size: 12px;">
        <?= session()->getFlashdata('sukses') ?>
    </div>
<?php endif ?>
      <?php
      $count =0;
    foreach ($header as $key =>
      $value) { {$count++; if ($count == 2) {
        break;}} ?>
      
      <div class="content_brg">
        <div class="header">
        <img src="/assets/image/Logo Morgan.png" alt="" width="100px">
        <h3>STRUCK PEMESANAN</h3>
        <small><?= $value['created_at']; ?></small>
        <h6> <span id="statusp">Status Pembayaran : </span> <span id="status"><?= $value['status']; ?></span> </h6>
        <?php
    foreach ($pemesan as $key =>
      $value) { // var_dump($user); ?>
        <p id="username"> <span id="nama">Pelanggan : </span>
          <?= $value['firstname']; ?>
          <?= $value['lastname']; ?>
        </p>
        <?php
    }

    ?>
      </div>
      <span id="batas">==========================</span>
      <?php $total = 0; ?>
      <?php
      $id = $uri->getSegment(2);
    foreach ($pemesanan as $key =>
      $value) { // var_dump($user); ?>
      <?php if ($value['id_users'] != $id): ?>
        <?php continue; ?>
    <?php endif ?>
      <div class="content">
        <p id="nama_barang"> <?= $value['Nama_barang']; ?></p>
        <div class="sub-content">
        <p id="harga">  <?= number_to_currency($value['harga'], 'IDR'); ?></p>
        <p id="jumlah"> <span id="jumlah">x </span> <?= $value['jumlah']; ?></p>
        <p id="total"><?= number_to_currency($value['harga'] * $value['jumlah'], 'IDR'); ?></p>
        
        <?php $total += $value['harga'] * $value['jumlah'];?>
        </div>
        </div>
        <?php
    }

    ?>
        <span id="batas">==========================</span>
        <div class="footer">
        <p id="total"> <span id="totall">Total Harga : </span> 
         <?= number_to_currency($total, 'IDR');  ?>
        </p>
      </div>
      </div>
      <?php
    }

    ?>
    </div>
<div class="check">
  <div class="pengantaran">
    <p>Pilih Metode Pengantaran :</span></p>
    <div class="method-pengantaran">
      <button onclick="ambil()" id="ambil">Ambil</button>
      <button onclick="antar()" id="antar">Antar</button>
    </div>
    <div class="alamat-toko" id="captain">
      <h6>Alamat Captain Chicken : </h6>
      <p>5CP6+QRF, Kalangan, Baturetno, Kec. Banguntapan, Kabupaten Bantul, Daerah Istimewa Yogyakarta 55197</p>
      <a href="https://maps.app.goo.gl/KzKVcwKTwAxKx1iz5">View Maps</a>
    </div>
    <div class="alamat" id="alamat">
      <form action="<?= base_url('upalamat/'.$uri->getSegment(2))?>" method="post">
        <input type="text" id="alamat" name="alamat" placeholder="Masukan Alamat Anda :" >
        <input type="text" id="nohp" name="nohp" placeholder="Masukan Nomor HP Anda :">
        <input type="datetime-local" id="waktu" name="waktu" >
        <?= csrf_field() ?>
        <button type="submit">Simpan</button>
      </form>
    </div>
  </div>
  <div class="pembayaran">
      <p>Pilih Metode Pembayaran : </p>
      <div class="method-pengantaran">
      <button onclick="cash()" id= cash>Cash</button>
      <button onclick="transfer()" id="transfer">Transfer</button>
      
    </div>
    <div class="tranfer" id="transfer-con">
      <h6>Bank Tekotok</h6>
      <h3 id="norek">01927656637 <span id="span-copy"><button onclick="copy()" id="copy">Copy</button></span></h3>
      <p>A.N Syaiful ihsan</p>
       
      <?php
      $count=0;
      $id = $uri->getSegment(2);
    foreach ($pemesanan as $key =>
      $value) { // var_dump($user); ?>
      <?php if ($value['id_users'] != $id): ?>
        <?php continue; ?>
    <?php endif ?>
    <?php  {$count++; if ($count == 2) {
        break;}} ?>
      <form action="<?= base_url('buktitf/'.$uri->getSegment(2))?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="total" id="total" value="<?= number_to_currency($total, 'IDR');  ?>">
        <input type="hidden" name="nama" id="nama" value="<?= $value['firstname'] .' '.$value['lastname'] ?>">
        <input type="hidden" name="id_users" id="id_users" value="<?= $value['id_users']?>">
        <input type="file" name="bukti" id="bukti" class="file-input">
        <?= csrf_field() ?>
        <button type="submit" id="kirim" style="border: none;
    outline: none;
    background-color: rgb(239, 154, 83);
    padding: 5px 15px;
    border-radius: 5px;
    color: white;
    font-size: 12px;
    margin-top: 10px;">Kirim</button>
      </form>
      <?php
    }

    ?>
    </div>
  </div>
</div>
<form action="<?= base_url('emailuser/'.$uri->getSegment(2))?>" method="post">
<div class="pesan">
    
    <?php
    $count=0;
    foreach ($header as $key =>
      $value) { ?>
       <?php if ($value['id_users'] != $id): ?>
        <?php continue; ?>
    <?php endif ?>
    <?php  {$count++; if ($count == 2) {
        break;}} ?>
         
         <input type="hidden" name="nama" value="<?= $value['firstname'].' '.$value['lastname'] ?>">
        <input type="hidden" name="email" value="<?= $value['email'] ?>">
        <input type="hidden" name="total" id="total" value="<?= number_to_currency($total, 'IDR');  ?>">
        
    
        <?php
        }
        ?>
  
  <button type="submit">Pesan sekarang</button>

</div>
</form>
<script>
  let text = document.getElementById('norek').innerHTML;
  const copy = async () => {
    try {
      await navigator.clipboard.writeText(text);
      console.log('Content copied to clipboard');
    } catch (err) {
      console.error('Failed to copy: ', err);
    }
    document.getElementById('copy').innerHTML='Copied';
    document.getElementById('copy').style.backgroundColor='rgb(189, 88, 5)';
  }

</script>
    <script>
      var pembayaran = document.getElementById('status').innerHTML;
      var lunas = 'Lunas';
      console.log(pembayaran);
      if (pembayaran == lunas) {
        document.getElementById('status').style.color='green';
      }
      else{
        document.getElementById('status').style.color='red';
      }
    </script>
    <script>
      
      function ambil() {
        var ambil = document.getElementById('ambil');
        var antar = document.getElementById('antar');
        var alamat = document.getElementById('alamat');
        var captain = document.getElementById('captain');
        
        
        
        alamat.style.position = 'absolute';
        alamat.style.visibility = 'hidden';
        captain.style.position = 'relative';
        captain.style.visibility = 'visible';
        ambil.style.backgroundColor = 'rgb(189, 88, 5)';
        antar.style.visibility = 'hidden';
        if (ambil.style.backgroundColor == 'rgb(189, 88, 5)') {
          <?php 
        $db = \Config\Database::connect();
        $id = $uri->getSegment(2);
        $ambil = 'ambil';
        $subquery = $db->table('pemesanan')->set('pengantaran',$ambil)->where('id_users', $id)->update();
        
      ?>
        };
      }
      </script>
      



    <script>


      function cash() {
        var trans = document.getElementById('transfer-con');
        var transfer = document.getElementById('transfer');
        var cash = document.getElementById('cash');
        
        trans.style.position = 'absolute';
        trans.style.visibility = 'hidden';
        transfer.style.visibility = 'hidden';
        cash.style.backgroundColor = 'rgb(189, 88, 5)';
        if (cash.style.backgroundColor = 'rgb(189, 88, 5)') {
          <?php 
        
        $pembayaran = 'cash';
        $subquery = $db->table('pemesanan')->set('pembayaran',$pembayaran)->where('id_users', $id)->update();
        
      ?>
        };
      }
    </script>
    <script>
      $(document).ready(function () {
  $(".first-button").on("click", function () {
    $(".animated-icon1").toggleClass("open");
  });
});

function bar() {
  var x = document.getElementById("navbarr");
  if (x.style.display == "grid") {
    x.style.display = "none";
  } else {
    x.style.display = "grid";
  }
}
    </script>
  </body>
</html>
 