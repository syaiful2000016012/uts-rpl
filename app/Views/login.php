<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="/assets/css/login.css">
  <title></title>

</head>

<body>
  <div class="logo">
    <img src="/assets/image/Logo Morgan.png" alt="">
  </div>
  


        <div class="container" id="con-form">
          <h3>Login</h3>
          <hr>
          <?php if (session()->get('success')) : ?>
            <div class="alert alert-success" role="alert">
              <?= session()->get('success') ?>
            </div>
          <?php endif; ?>
          <form class="" action="/" method="post">
            <div class="sub-con">
              <label for="email">Email address</label>
              <input type="text" class="form-control" name="email" id="email" value="<?= set_value('email') ?>">
            </div>
            <div class="sub-con">
              <label for="password">Password</label>
              <input type="password" class="form-control" name="password" id="password" value="">
            </div>
            <?php if (isset($validation)) : ?>
              <div class="col-12">
                <div class="alert alert-danger" role="alert">
                  <?= $validation->listErrors() ?>
                </div>
              </div>
            <?php endif; ?>
            <div class="row" id="btn">
              <div class="col-12 col-sm-8 text-left" id="reg">
                <a href="/register">Don't have an account yet?</a>
              </div>
              <div class="col-12 col-sm-4">
                <button type="submit" >Login</button>
              </div>
              
            </div>
          </form>



  </div>