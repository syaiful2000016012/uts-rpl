<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.6.2.js" integrity="sha256-pkn2CUZmheSeyssYw3vMp1+xyub4m+e+QK4sQskvuo4=" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="/assets/css/dashboard.css">
  <script src="/assets/js/dashboarduser.js"></script>
  <title></title>

</head>

<body>
  <?php
  $uri = service('uri');
  ?>
  <nav class="navbar navbar-expand-lg navbar">
    <div class="container">
      <a class="navbar-brand" href="/">Captain <span id="ciken">Chicken</span> </a>
      <button class="navbar-toggler first-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent20" aria-controls="navbarSupportedContent20" aria-expanded="false" aria-label="Toggle navigation" id="burger">
        <div class="animated-icon1"><span></span><span></span><span></span></div>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent20">
        <?php if (session()->get('isLoggedIn')) : ?>
          <ul class="navbar-nav mr-auto">
            <li class="nav-item <?= ($uri->getSegment(1) == 'dashboard' ? 'active' : null) ?>">
              <a class="nav-link" href="/dashboard">Home</a>
            </li>
            <li class="nav-item <?= ($uri->getSegment(1) == 'dashboard' ? 'active' : null) ?>">
              <a class="nav-link" href="/adm">Pesanan</a>
            </li>
            <li class="nav-item <?= ($uri->getSegment(1) == 'profile' ? 'active' : null) ?>">
              <a class="nav-link" href="/profile">Profile</a>
            </li>
          </ul>
          <ul class="navbar-nav my-2 my-lg-0">
            <li class="nav-item">
              <a class="nav-link" href="/logout">Logout</a>
            </li>
          </ul>
        <?php else : ?>
          <ul class="navbar-nav mr-auto">
            <li class="nav-item <?= ($uri->getSegment(1) == '' ? 'active' : null) ?>">
              <a class="nav-link" href="/">Login</a>
            </li>
            <li class="nav-item <?= ($uri->getSegment(1) == 'register' ? 'active' : null) ?>">
              <a class="nav-link" href="/register">Register</a>
            </li>
          </ul>
        <?php endif; ?>
      </div>
    </div>
  </nav>
  <div class="container">
    <div class="row">
      <div class="greeting">
        <h1>Hello, Admin <span id="sub-greet"> <?= session()->get('firstname') ?> <?= session()->get('lastname') ?></span></h1>
      </div>
    </div>
    
  </div>
  <div class="stock">
    <div class="barang">
      <h1 id="daftar">Daftar barang</h1>
      <a href="/Dashboard/create" ><button id="tambah_barang">Tambah Barang</button> </a>
      
      <div class="dftbrg">
        <?php
    foreach ($data as $user) {
      // var_dump($user);
    ?>
        <div class="isi_brg">
          <div class="header_brg">
            <img src="/assets/image/<?php echo $user['thumb']; ?>" alt="" />
          </div>
          <div class="content_brg">
            <h3 id="tittle_brg"><?php echo $user['Nama_barang']; ?></h3>
            <p><?php echo $user['desc']; ?></p>
            <h4>Harga</h4>
            <p id="harga"><?php echo $user['harga']; ?></p>
            <h4>Jumlah</h4>
            <p><?php echo $user['stok']; ?></p>
          </div>
          <div class="edit_brg">
            <a href="<?= base_url('Dashboard/update/'.$user['id_barang']) ?>"><button>Edit</button></a>
            <a href="<?= base_url('/delete/'.$user['id_barang']) ?>" onclick="return confirm('Are you sure ?')"><button>Hapus</button></a>
          </div>
        </div>
        <?php
    }

    ?>
    
      </div>
    </div>
  </div>
  