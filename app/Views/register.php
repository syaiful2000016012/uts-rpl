<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
      crossorigin="anonymous"
    />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap"
      rel="stylesheet"
    />
    <link rel="stylesheet" href="/assets/css/register.css" />
    <title>Document</title>
  </head>
  <body>
    <div class="logo">
      <img src="/assets/image/Logo Morgan.png" alt="" />
    </div>

    <div class="container" id="con-form">
      <h3>Register</h3>
      <hr />
      <form class="" action="/register" method="post">
        <div class="row">
          <div class="col-12 col-sm-6">
            <div class="form-group">
              <label for="firstname">First Name</label>
              <input
                type="text"
                class="form-control"
                name="firstname"
                id="firstname"
                value="<?= set_value('firstname') ?>"
              />
            </div>
          </div>
          <div class="col-12 col-sm-6">
            <div class="form-group">
              <label for="lastname">Last Name</label>
              <input
                type="text"
                class="form-control"
                name="lastname"
                id="lastname"
                value="<?= set_value('lastname') ?>"
              />
            </div>
          </div>
          <div class="col-12">
            <div class="form-group">
              <label for="email">Email address</label>
              <input
                type="text"
                class="form-control"
                name="email"
                id="email"
                value="<?= set_value('email') ?>"
              />
            </div>
          </div>
          <div class="col-12 col-sm-6">
            <div class="form-group">
              <label for="password">Password</label>
              <input
                type="password"
                class="form-control"
                name="password"
                id="password"
                value=""
              />
            </div>
          </div>
          <div class="col-12 col-sm-6">
            <div class="form-group">
              <label for="password_confirm">Confirm Password</label>
              <input
                type="password"
                class="form-control"
                name="password_confirm"
                id="password_confirm"
                value=""
              />
            </div>
          </div>
          <?php if (isset($validation)): ?>
          <div class="col-12">
            <div class="alert alert-danger" role="alert">
              <?= $validation->listErrors() ?>
            </div>
          </div>
          <?php endif; ?>
        </div>

        <div class="row">
          <div class="col-12 col-sm-8 text-left" id="reg">
            <a href="/">Already have an account</a>
          </div>
          <div class="col-12 col-sm-4">
            <button type="submit" class="btn btn-primary">Register</button>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>
