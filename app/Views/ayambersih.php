<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.6.2.js" integrity="sha256-pkn2CUZmheSeyssYw3vMp1+xyub4m+e+QK4sQskvuo4=" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/scrollreveal"></script>
    <link rel="stylesheet" href="/assets/css/ayambersih.css">

    <title>Katalog Ayam</title>
</head>
<style>
  #navbarr {
  display: none;
}
</style>
<body>
  <?php
  $uri = service('uri');
  ?>
  <nav class="navbar navbar-expand-lg navbar">
    <div class="container">
      <a class="navbar-brand" href="/dashboardUser/<?= ($uri->getSegment(2))?>">Captain <span id="ciken">Chicken</span> </a>
      <button class="navbar-toggler first-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent20" aria-controls="navbarSupportedContent20" aria-expanded="false" aria-label="Toggle navigation" id="burger"  onclick="bar()">
        <div class="animated-icon1"><span></span><span></span><span></span></div>
      </button>
      <div class="navbar-collapse" id="navbarr">
        <?php if (session()->get('isLoggedIn')) : ?>
          <ul class="navbar-nav mr-auto">
            <li class="nav-item <?= ($uri->getSegment(2) == 'dashboard' ? 'active' : null) ?>">
              <a class="nav-link" href="/dashboardUser/<?= ($uri->getSegment(2))?>">Home</a>
            </li>
            <li class="nav-item <?= ($uri->getSegment(2) == 'dashboard' ? 'active' : null) ?>">
              <a class="nav-link" href="/pesanan/<?= ($uri->getSegment(2))?>">Pesanan</a>
            </li>
            <li class="nav-item <?= ($uri->getSegment(2) == 'profile' ? 'active' : null) ?>">
              <a class="nav-link" href="/profile/<?= ($uri->getSegment(2))?>">Profile</a>
            </li>
          </ul>
          <ul class="navbar-nav my-2 my-lg-0">
            <li class="nav-item">
              <a class="nav-link" href="/logout">Logout</a>
            </li>
          </ul>
        <?php else : ?>
          <ul class="navbar-nav mr-auto">
            <li class="nav-item <?= ($uri->getSegment(2) == '' ? 'active' : null) ?>">
              <a class="nav-link" href="/">Login</a>
            </li>
            <li class="nav-item <?= ($uri->getSegment(2) == 'register' ? 'active' : null) ?>">
              <a class="nav-link" href="/register">Register</a>
            </li>
          </ul>
        <?php endif; ?>
      </div>
    </div>
  </nav>
  <div class="stock">
    <div class="barang">
        <h1 id="daftar">Daftar barang</h1>
        <?php if (session()->getFlashdata('success')): ?>
    <div class="alert alert-success">
        <?= session()->getFlashdata('success') ?>
    </div>
<?php endif ?>
        <div class="dftbrg">
          <?php
    foreach ($ayam as $key =>
        $value) {
      // var_dump($user);
    ?>
          <div class="isi_brg">
            <div class="header_brg">
              <img src="/assets/image/<?= $value['thumb']; ?>" alt="" />
            </div>
            <div class="content_brg">
              <h3 id="tittle_brg"><?= $value['Nama_barang']; ?></h3>
              <p><?= $value['desc']; ?></p>
              <h4>Harga</h4>
              <p id="harga"><?= number_to_currency($value['harga'], 'IDR'); ?></p>
              <h4>Jumlah</h4>
              <p><?= $value['stok']; ?></p>
            </div>
            <div class="input-group">
              <form action="<?= base_url('/pesan/'.$uri->getSegment(2))?>" method="post">
              <?php 
              $db = \Config\Database::connect();
              $seg = $uri->getSegment(2);
              
              ?>
              <div class="idbrg">
                <input
                  type="text"
                  name="id_barang"
                  id="id_barang"
                  value="<?= $value['id_barang']; ?>"
                />
               </div>
               <input
                  type="hidden"
                  name="stok"
                  id="stok"
                  value="<?= $value['stok']; ?>"
                  
                />
                <input
                  type="text"
                  name="quantity"
                  id="quantity"
                  value=""
                  placeholder="Masukan berat"
                />
                <button type="submit">Beli</button>
              </form>
            </div>
           
          </div>
           <?php
            }
            ?>
        </div>
        <div class="checkout">
          <a href="<?= base_url('/pemesanan/'.$uri->getSegment(2))?>">checkout</a>
        </div>
        
      </div>
    </div>
  <script>


    var i = 1;
function kurang() {
  document.getElementById("quantity").value = i--;
  if (i < 0) {
    i = 0;
  }
}
function tambah() {
  document.getElementById("quantity").value = i++;
}

$(document).ready(function () {
  $(".first-button").on("click", function () {
    $(".animated-icon1").toggleClass("open");
  });
});

function bar() {
  var x = document.getElementById("navbarr");
  if (x.style.display == "grid") {
    x.style.display = "none";
  } else {
    x.style.display = "grid";
  }
}


    
</script>
</body>
</html>