<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
      crossorigin="anonymous"
    />
    <script
      src="https://code.jquery.com/jquery-3.6.2.js"
      integrity="sha256-pkn2CUZmheSeyssYw3vMp1+xyub4m+e+QK4sQskvuo4="
      crossorigin="anonymous"
    ></script>
    <script src="https://unpkg.com/scrollreveal"></script>
    <link rel="stylesheet" href="/assets/css/adm.css" />

    <title>Document</title>
  </head>
  <style>
  #navbarr {
  display: none;
}
</style>
  <body>
    <?php
  $uri = service('uri');
  ?>
    <nav class="navbar navbar-expand-lg navbar">
    <div class="container">
      <a class="navbar-brand" href="/dashboardUser/<?= ($uri->getSegment(2))?>">Captain <span id="ciken">Chicken</span> </a>
      <button class="navbar-toggler first-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent20" aria-controls="navbarSupportedContent20" aria-expanded="false" aria-label="Toggle navigation" id="burger"  onclick="bar()">
        <div class="animated-icon1"><span></span><span></span><span></span></div>
      </button>
      <div class="navbar-collapse" id="navbarr">
        <?php if (session()->get('isLoggedIn')) : ?>
          <ul class="navbar-nav mr-auto">
            <li class="nav-item <?= ($uri->getSegment(2) == 'dashboardUser' ? 'active' : null) ?>">
              <a class="nav-link" href="/dashboard">Home</a>
            </li>
            <li class="nav-item <?= ($uri->getSegment(2) == 'pemesanan' ? 'active' : null) ?>">
              <a class="nav-link" href="/adm">Pesanan</a>
            </li>
            <li class="nav-item <?= ($uri->getSegment(2) == 'profile' ? 'active' : null) ?>">
              <a class="nav-link" href="/profile">Profile</a>
            </li>
          </ul>
          <ul class="navbar-nav my-2 my-lg-0">
            <li class="nav-item">
              <a class="nav-link" href="/logout">Logout</a>
            </li>
          </ul>
        <?php else : ?>
          <ul class="navbar-nav mr-auto">
            <li class="nav-item <?= ($uri->getSegment(2) == '' ? 'active' : null) ?>">
              <a class="nav-link" href="/">Login</a>
            </li>
            <li class="nav-item <?= ($uri->getSegment(2) == 'register' ? 'active' : null) ?>">
              <a class="nav-link" href="/register">Register</a>
            </li>
          </ul>
        <?php endif; ?>
      </div>
    </div>
  </nav>
  <div class="content">
    <?php if (session()->getFlashdata('lunas')): ?>
    <div class="alert alert-success" style="font-size: 12px;">
        <?= session()->getFlashdata('lunas') ?>
    </div>
<?php endif ?>
<?php if (session()->getFlashdata('antar')): ?>
    <div class="alert alert-success" style="font-size: 12px;">
        <?= session()->getFlashdata('antar') ?>
    </div>
<?php endif ?>
<?php if (session()->getFlashdata('selesai')): ?>
    <div class="alert alert-success" style="font-size: 12px;">
        <?= session()->getFlashdata('selesai') ?>
    </div>
<?php endif ?>
    <?php
    foreach ($user as $key => $value) {
    ?>
    <div class="sub_content">
        <div class="user">
            <p><?= $value['firstname'].' '.$value['lastname']. ' | '.$value['id_users']?> </p>
        </div>
        <div class="navigasi">
            <a href="<?= base_url('view/'.$value['id_users']) ?>"><button>View</button></a>
            <form action="<?= base_url('confirm/'.$value['id_users']) ?>" method="post">
            <input type="hidden" name="email" value="<?= $value['email'] ?>">
            <input type="hidden" name="nama" value="<?= $value['firstname'].$value['lastname'] ?>">
            <button type="submit">Konfirm</button>
        </form>
            <form action="<?= base_url('antar/'.$value['id_users']) ?>" method="post">
            <input type="hidden" name="email" value="<?= $value['email'] ?>">
            <input type="hidden" name="nama" value="<?= $value['firstname'].' '.$value['lastname'] ?>">
            <button type="submit">antar</button>
        </form>
             <a href="<?= base_url('selesai/'.$value['id_users']) ?>"><button>selesai</button></a>
        </div>
    </div>
    <?php } ?>
  </div>

  <script>
      $(document).ready(function () {
  $(".first-button").on("click", function () {
    $(".animated-icon1").toggleClass("open");
  });
});

function bar() {
  var x = document.getElementById("navbarr");
  if (x.style.display == "grid") {
    x.style.display = "none";
  } else {
    x.style.display = "grid";
  }
}
    </script>
</body>
</html>