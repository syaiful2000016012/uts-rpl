<div class="container p-3 mt-3 bg-light " style="border-radius: 10px;" id="conn">
    <form action=" /create" method="post" enctype="multipart/form-data">
        <div class="row mb-3">
            <label for="Nama_barang" class="col-sm-2 col-form-label col-form-label-lg">Nama barang</label>
            <div class="col-sm-10">
                <input type="text" class="form-control form-control-lg" name="Nama_barang" id="Nama_barang" placeholder="Nama barang" autocomplete="off">
            </div>
        </div>
        <div class="row mb-3">
            <label for="thumb" class="col-sm-2 col-form-label col-form-label-lg">Thumb</label>
            <div class="col-sm-10">
                <input type="file" class="form-control form-control-lg" name="thumb" id="thumb" placeholder="Thumb" autocomplete="off">
            </div>
        </div>
        <div class="row mb-3">
            <label for="desc" class="col-sm-2 col-form-label col-form-label-lg">Desc</label>
            <div class="col-sm-10">
                <input type="text" class="form-control form-control-lg" name="desc" id="desc" placeholder="Desc" autocomplete="off">
            </div>
        </div>
        <div class="row mb-3">
            <label for="harga" class="col-sm-2 col-form-label col-form-label-lg">Harga</label>
            <div class="col-sm-10">
                <input type="number" class="form-control form-control-lg" name="harga" id="harga" placeholder="Harga" autocomplete="off">
            </div>
        </div>
        <div class="row mb-3">
            <label for="stok" class="col-sm-2 col-form-label col-form-label-lg">Stok</label>
            <div class="col-sm-10">
                <input type="text" class="form-control form-control-lg" name="stok" id="stok" placeholder="Stok" autocomplete="off">
            </div>
        </div>
        <div class="row mb-3">
            <label for="tipe" class="col-sm-2 col-form-label col-form-label-lg">tipe</label>
            <div class="col-sm-10">
                <input type="radio" id="bersih" name="tipe" value="bersih">
                <label for="bersih">Bersih</label><br>
                <input type="radio" id="kotor" name="tipe" value="kotor">
                <label for="kotor">Kotor</label><br>
            </div>
        </div>
        
        <div class="row">
            <div class="float-div">
                <button type="submit" id="tambah-btn">Tambah</button>
                <a href="<?= base_url('Dashboard/index') ?>" id="cncl-btn"><Button>Cancel</Button></a>
            </div>
        </div>
    </form>

</div>