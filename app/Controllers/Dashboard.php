<?php

namespace App\Controllers;

use App\Models\DashboardModel;
use App\Models\UserModel;
use App\Models\PemesananModel;

class Dashboard extends BaseController
{
	public function __construct()
	{
		helper('number');
	}
	public function index()
	{
		$dashboardModel = new DashboardModel();
		$data = $dashboardModel->findAll();

		echo view('templates/header', $data);
		echo view('dashboard', compact('data'));
		echo view('templates/footer');
	}
	public function create()
	{
		$dashboardModel = new DashboardModel();
		$data = $dashboardModel->findAll();

		echo view('templates/header', $data);
		echo view('dashboard', compact('data'));
		echo view('templates/create');
	}
	public function update($id=null)
	{
		$dashboardModel = new DashboardModel();
		$data['update'] = $dashboardModel->where('id_barang',$id) -> first();

		echo view('templates/header', $data);
		echo view('dashboard', compact('data'));
		echo view('templates/update');
	}
	public function ayambersih($id=null)
	{
		$userModel = new UserModel();
		$dashboardModel = new DashboardModel();
		$pemesanan = new PemesananModel();
		$data = $userModel->where("id_users", $id);
		$data = [
			'title' => 'List Pemesanan',
			'pemesanan' => $pemesanan->get_pemesanan(),
			'ayam' => $dashboardModel->where("tipe", "bersih")->findAll(),
			'isi' => 'pemesanan',
		];
		echo view('ayambersih',$data);
	}
	public function ayamkotor($id=null)
	{
		$userModel = new UserModel();
		$dashboardModel = new DashboardModel();
		$pemesanan = new PemesananModel();
		$data = $userModel->where("id_users", $id);
		$data = [
			'title' => 'List Pemesanan',
			'pemesanan' => $pemesanan->get_pemesanan(),
			'ayam' => $dashboardModel->where("tipe", "kotor")->findAll(),
			'isi' => 'pemesanan',
		];
		echo view('ayamkotor',$data);
		
	}
	
	//--------------------------------------------------------------------

}
