<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use App\Models\DashboardModel;
use App\Models\UserModel;
use App\Models\PemesananModel;
use App\Models\PembayaranModel;
use CodeIgniter\Session\Session;
use CodeIgniter\I18n\Time;

class AdminDashboard extends BaseController
{
    public function __construct()
	{
		$this->Pemesanan = new PemesananModel();
		$this->user = new UserModel();
		$this->bayar = new PembayaranModel();
		$this->email = \Config\Services::email();
		
		helper('number');
		helper('input');
		helper('url');

	}
    public function tambahdata()
    {
        $model = new DashboardModel();
        $filethumb = $this->request->getFile('thumb');
        $filethumb->move('assets/image');
        $namathumb = $filethumb->getName();
        $newData = [
            'Nama_barang' => $this->request->getVar('Nama_barang'),
            'thumb' => $namathumb,
            'desc' => $this->request->getVar('desc'),
            'harga' => $this->request->getVar('harga'),
            'stok' => $this->request->getVar('stok'),
            'tipe' => $this->request->getVar('tipe')
        ];
        $model->save($newData);
        return redirect('dashboard');

        $session = session();
        $session->setFlashdata('success', 'Successful Saved Data');
    }
    public function update($id=null)
    {
        $model = new DashboardModel();
        $id = $this->request->uri->getSegment(2);
        $data = $model->update($id, [
            "Nama_barang" => $this->request->getPost('Nama_barang'),
            "thumb" => $this->request->getPost('thumb'),
            "desc" => $this->request->getPost('desc'),
            "harga" => $this->request->getPost('harga'),
            "stok" => $this->request->getPost('stok'),
            "tipe" => $this->request->getVar('tipe')
        ]);
        
        return redirect('dashboard')->with('success', 'Data updated Successfully');
    }
    public function delete($id)
    {
        $id = $this->request->uri->getSegment(2);
        $model = new DashboardModel();
        $data = $model->delete($id);

        
    return redirect('dashboard')->with('success', 'Data Deleted Successfully');
    }
    public function pesanan_adm()
	{
		$id = $this->request->uri->getSegment(2);
		$data = [
			'title' => 'List Pemesanan',   
			'pemesanan' => $this->Pemesanan->get_pemesanan(),
			'user' => $this->user->where('role','user')->findAll(),
			'bayar' => $this->bayar->find(),
			'isi' => 'pemesanan',
		];
		$session = session();
		echo view('pesanan_adm',$data);
		
	}
    public function adm_view($id=null)
	{
		$id = $this->request->uri->getSegment(2);
		$data = [
			'title' => 'List Pemesanan',   
			'pemesanan' => $this->Pemesanan->get_pemesanan(),
			'user' => $this->user->where('role','user')->findAll(),
			'bayar' => $this->bayar->find(),
			'isi' => 'pemesanan',
		];
		$session = session();
        echo view('templates/adm_view',$data);
		echo view('pesanan_adm',$data);
		
	}
    public function adm_confirm($id=null){
        $email = \Config\Services::email();
		$nama = $this->request->getPost('nama');
		$emailuser = $this->request->getPost('email');
        $myTime = new Time('now');

		$email->setFrom('shznnn2201@gmail.com', 'Captain Chicken');
		$email->setTo($emailuser);

		$email->setSubject('Pembayaran Telah Terverivikasi');
        $image = '<div style="display:flex;width:100%;justify-content:center;margin:0px 30px"> <img src="https://raw.githubusercontent.com/Shznnn/tekweb2022/main/Logo%20Morgan.png" style="width:100px; height:auto;"></div>';
		$email->setMessage('<html><body style=background-color:#fff5e6;padding:30px 50px;border-radius:20px;>'.$image.'<br><h2>Hai, '.$nama.'</h2>'.' <br><p>Selamat, pembayaran anda telah terverifikasi oleh admin.<br> Pada Tanggal : '.$myTime.'</p>'.'<br><br>Mengalami kendala?<br>'.'<a href="http://wa.me/+6285727442718" style="text-decoration:none;color:white;"><button style="background-color: rgb(239, 154, 83);border-radius: 5px;border: none;outline: none;color:white;padding:5px 10px;margin-top:20px;">Hubungi Admin</button></a>'.'</body></html>');
		$email->send();


        $data = [
			'status_bayar' => 'lunas',
		];
		
		$this->bayar->set($data)->where('id_users',$id);
		$this->bayar->update();
		$session = Session();
		$session->setFlashdata('lunas', 'Pembayaran Telah Diverifikasi.');
		return redirect()->back()->withInput();  
    }
    public function adm_antar($id=null){
        $email = \Config\Services::email();
		$nama = $this->request->getPost('nama');
		$emailuser = $this->request->getPost('email');
        $myTime = new Time('now');

		$email->setFrom('shznnn2201@gmail.com', 'Captain Chicken');
		$email->setTo($emailuser);

		$email->setSubject('Pesanan anda sedang diantar');
        $image = '<div style="display:flex;width:100%;justify-content:center;margin:0px 30px"> <img src="https://raw.githubusercontent.com/Shznnn/tekweb2022/main/Logo%20Morgan.png" style="width:100px; height:auto;"></div>';
		$email->setMessage('<html><body style=background-color:#fff5e6;padding:30px 50px;border-radius:20px;>'.$image.'<br><h2>Hai, '.$nama.'</h2>'.' <br><p>Pesanan anda sedang diantar oleh kurir.<br> Pada Tanggal : '.$myTime.'<br><br>Terima kasih telah berbelanja di Captain Chicken.<br>Semoga anda puas dengan pelayanan kami :)</p>'.'<br><br>Mengalami kendala?<br>'.'<a href="http://wa.me/+6285727442718" style="text-decoration:none;color:white;"><button style="background-color: rgb(239, 154, 83);border-radius: 5px;border: none;outline: none;color:white;padding:5px 10px;margin-top:20px;">Hubungi Admin</button></a>'.'</body></html>');
		$email->send();





        $data = [
			'status_pesanan' => 'diantar',
		];
		
		$this->bayar->set($data)->where('id_users',$id);
		$this->bayar->update();
		$session = Session();
		$session->setFlashdata('antar', 'Status Pesanan Telah Diubah = Pengantaran.');
		return redirect()->back()->withInput();  
    }
    public function adm_selesai($id=null){
        $data = [
			'status_pesanan' => 'selesai',
		];
		
		$this->bayar->set($data)->where('id_users',$id);
		$this->bayar->update();
		$session = Session();
		$session->setFlashdata('selesai', 'Status Pesanan Telah Diubah = Selesai.');
		return redirect()->back()->withInput();  
    }
}
