<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use App\Models\DashboardModel;
use App\Models\UserModel;
use App\Models\PemesananModel;
use App\Models\PembayaranModel;
use CodeIgniter\Session\Session;
use CodeIgniter\I18n\Time;

class Pemesanan extends BaseController
{
	public function __construct()
	{
		$this->Pemesanan = new PemesananModel();
		$this->user = new UserModel();
		$this->bayar = new PembayaranModel();
		$this->barang = new DashboardModel();
		$this->email = \Config\Services::email();
		
		helper('number');
		helper('input');
		helper('url');

	}
	
	public function index($id=null)
	{
		
		$data = [
			'title' => 'List Pemesanan',
			'header' => $this->Pemesanan->where('id_users',$id)->get_pemesanan(),
			'pemesanan' => $this->Pemesanan->where('id_users',$id)->get_pemesanan(),
			'pemesan' => $this->user->where('id_users',$id)->find(),
			'isi' => 'pemesanan',
		];
		$session = session();
		echo view('pemesanan',$data);
		
	}
	public function pesanan($id=null)
	{
		$id = $this->request->uri->getSegment(2);
		$data = [
			'title' => 'List Pemesanan',
			'header' => $this->Pemesanan->where('id_users',$id)->get_pemesanan(),
			'pemesanan' => $this->Pemesanan->where('id_users',$id)->get_pemesanan(),
			'pemesan' => $this->user->where('id_users',$id)->find(),
			'bayar' => $this->bayar->where('id_users',$id)->find(),
			'isi' => 'pemesanan',
		];
		$session = session();
		echo view('pesanan',$data);
		
	}
	public function pesan($id=null)
	{
		$idu = $this->request->uri->getSegment(2);
		helper('form');
		$stok = $this->request->getPost('stok');
		$jumlah = $this->request->getPost('quantity');
		$id_brg = $this->request->getVar('id_barang');
		$newdata =[
			'id_users' => $id,
			'id_barang' => $id_brg,
			'jumlah' => $jumlah,
		];
		$data = [
			'stok' => $stok-$jumlah,
		];

		$session = Session();
		$session->setFlashdata('success', 'Barang berhasil ditambahkan!');
		$this->barang->set($data)->where('id_barang',$id_brg);
		$this->barang->update();
		$this->Pemesanan->save($newdata);

		return redirect()->back()->withInput(); 
	}
	public function alamatpesan($id=null){
		helper('form');
		
        $id = $this->request->uri->getSegment(2);
		$waktu = $this->request->getVar('waktu');
		$alamat = $this->request->getVar('alamat');
		$nohp = $this->request->getVar('nohp');
        
		$data = [
			'alamat' => $alamat,
			'nohp' => $nohp,
			'waktu' => $waktu,
		];
		
		$this->Pemesanan->set($data)->where('id_users',$id);
		$this->Pemesanan->update();


		$session = Session();
		$session->setFlashdata('succes', 'Alamat berhasil ditambahkan!<br>Silahkan Pilih Metode Pembayaran.');
		return redirect()->back()->withInput();  
	}
	public function bukti($id=null){
		$email = \Config\Services::email();
		$nama = $this->request->getPost('nama');
		$tf = $this->request->getFile('bukti');
		$idu = $this->request->getPost('id_users');
		$total = $this->request->getPost('total');
		$myTime = new Time('now');
		$filename = $tf->getname();
		$tf->move('assets/bukti');
		$filepath = ('assets/bukti/'. $filename);

		$email->setFrom('shznnn2201@gmail.com', 'Captain Chicken');
		$email->setTo('shznnn2201@gmail.com');

		$email->setSubject('Bukti Tranfer dari : '.$nama.'');
		
		$barang ='';
		$query =$this->Pemesanan->where('id_users',$id)->get_pemesanan();
		foreach ($query as $key => $value) {
			if ($value['id_users'] != $id): 
        	continue;
    		endif;
			$barang .='<br>'. $value['Nama_barang'].'<br>'.number_to_currency($value['harga'], 'IDR').' x '.$value['jumlah'].' = '.number_to_currency($value['harga']*$value['jumlah'], 'IDR') ;
			
		}
		
		$email->setMessage('Hai, Admin Ivall.<br> Pelanggan telah mengirimkan bukti Tranfer :<br> <br>Nama : '.$nama.'<br>ID : '.$idu.'<br>Tanggal : '.$myTime.'<br><br>Detail Pesanan :<br>'.$barang.'<br><br> Total Harga : '.$total.'<br>');
		$email->attach($filepath,'attachment', $filename);
		$email->send();

		$data = [
			'nama' => $nama,
			'id_users' => $idu,
			'bukti' => $filename,
			'created_at' => $myTime,
		];

		$this->bayar->insert($data);
		
		$session = Session();
		$session->setFlashdata('sukses', 'Data pembayaran telah terkirim! <br> Status akan diverifikasi Oleh Admin.');
		return redirect()->back()->withInput();

	}
	public function emailuser($id=null){
		$email = \Config\Services::email();
		$nama = $this->request->getPost('nama');
		$total = $this->request->getPost('total');
		$emailuser = $this->request->getPost('email');
		$wa = '<html><body><a href="http://wa.me/+6285727442718" style:"text-decoration:none;color:white;"><button style:"background-color: rgb(239, 154, 83);border-radius: 5px;border: none;outline: none;">Hubungi Admin</button></a></body></html>';
		$myTime = new Time('now');

		$email->setFrom('shznnn2201@gmail.com', 'Captain Chicken');
		$email->setTo($emailuser);

		$email->setSubject('Pesanan anda telah kami terima');

		$barang ='';
		$query =$this->Pemesanan->where('id_users',$id)->get_pemesanan();
		foreach ($query as $key => $value) {
			if ($value['id_users'] != $id): 
        	continue;
    		endif;
			$barang .='<br>'. $value['Nama_barang'].'<br>'.number_to_currency($value['harga'], 'IDR').' x '.$value['jumlah'].' = '.number_to_currency($value['harga']*$value['jumlah'], 'IDR').'<br>' ;
			
		}
		$image = '<div style="display:flex;width:100%;justify-content:center;margin:0px 30px"> <img src="https://raw.githubusercontent.com/Shznnn/tekweb2022/main/Logo%20Morgan.png" style="width:100px; height:auto;"></div>';
		$email->setMessage('<html><body style=background-color:#fff5e6;padding:30px 50px;border-radius:20px;>'.$image.'<br><h2>Hai, '.$nama.'</h2>'.' <br><p>Terimakasih telah berbelanja di Captain Chicken.<br> pada '.$myTime.'</p>'.'<br>Kami sangat menantikan kedatangan anda berikutnya.<br><br><h4>Berikut Detail Pesanan Anda:</h4>'.'<p>'.$barang.'</p><p>Dengan Total : <h4> '.$total.'</h4></p>'.'<br><br>Mengalami kendala?<br>'.'<a href="http://wa.me/+6285727442718" style="text-decoration:none;color:white;"><button style="background-color: rgb(239, 154, 83);border-radius: 5px;border: none;outline: none;color:white;padding:5px 10px;margin-top:20px;">Hubungi Admin</button></a>'.'</body></html>');
		
		$email->send();

		
		$session = Session();
		$session->setFlashdata('sukses', 'Data Pemesanan telah terkirim! <br> Semoga hari kamu menyenangkan '.$nama);
		$data = [
			'title' => 'List Pemesanan',
			'header' => $this->Pemesanan->where('id_users',$id)->get_pemesanan(),
			'pemesanan' => $this->Pemesanan->where('id_users',$id)->get_pemesanan(),
			'pemesan' => $this->user->where('id_users',$id)->find(),
			'bayar' => $this->bayar->where('id_users',$id)->find(),
			'isi' => 'pemesanan',
		];
		$data2 = [
			'id_users' =>$id,
			'nama' => $this->request->getPost('nama'),
		];
		$this->bayar->insert($data2);
		$session = Session();
		$session->setFlashdata('sukses', 'Pemesanan anda telah berhasil.<br>Invoice akan dikirimkan melalui email.');
		echo view('dashboardUser',$data);
		

	}
	public function sementara(){
		
	}
	//--------------------------------------------------------------------

}