<?php

namespace App\Controllers;

use App\Models\DashboardModel;
use App\Models\UserModel;


class DashboardUser extends BaseController
{
	public function __construct()
	{
		helper('number');
		$this->user = new UserModel();
	}
	public function index($id=null)
	{
		$data = [
			'pemesan' => $this->user->where('id_users',$id)->find(),
		];
		

;
		echo view('dashboardUser', $data);
		
		
	}
	
	public function create()
	{
		$dashboardModel = new DashboardModel();
		$data = $dashboardModel->findAll();

		echo view('templates/header', $data);
		echo view('dashboard', compact('data'));
		echo view('templates/create');
	}
	public function update($id=null)
	{
		$dashboardModel = new DashboardModel();
		$data['update'] = $dashboardModel->where('id',$id) -> first();

		echo view('templates/header', $data);
		echo view('dashboard', compact('data'));
		echo view('templates/update');
	}
	public function pesan($id=null)
	{
		$dashboardModel = new DashboardModel();
		$data['pesan'] = $dashboardModel->where('id_barang',$id) -> first();

		echo view('templates/header', $data);
		echo view('ayamkotor', compact('data'));
		echo view('templates/pesan');
	}
	
	//--------------------------------------------------------------------

}
