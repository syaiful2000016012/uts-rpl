<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes(true);

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Users');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Users::index', ['filter' => 'noauth']);
$routes->get('logout', 'Users::logout');
$routes->match(['get', 'post'], 'register', 'Users::register', ['filter' => 'noauth']);
$routes->match(['get', 'post'], 'profile', 'Users::profile', ['filter' => 'auth']);
$routes->get('dashboard', 'Dashboard::index', ['filter' => 'auth']);
$routes->get('adm', 'AdminDashboard::pesanan_adm', ['filter' => 'auth']);
$routes->get('view/(:segment)', 'AdminDashboard::adm_view/$1');
$routes->match(['get', 'post'],'confirm/(:segment)', 'AdminDashboard::adm_confirm/$1');
$routes->match(['get', 'post'],'antar/(:segment)', 'AdminDashboard::adm_antar/$1');
$routes->match(['get', 'post'],'selesai/(:segment)', 'AdminDashboard::adm_selesai/$1');
$routes->get('dashboardUser/(:segment)', 'DashboardUser::index/$1', ['filter' => 'auth']);
$routes->match(['post'], 'create', 'AdminDashboard::tambahdata',);
$routes->add('/update/(:segment)', 'AdminDashboard::update/$1');
$routes->get('/delete/(:segment)', 'AdminDashboard::delete/$1');

$routes->get('ayambersih/(:segment)', 'Dashboard::ayambersih/$1', ['filter' => 'auth']);
$routes->get('ayamkotor/(:segment)', 'Dashboard::ayamkotor/$1', ['filter' => 'auth']);

$routes->get('pemesanan/(:segment)', 'Pemesanan::index/$1');
$routes->get('pesanan/(:segment)', 'Pemesanan::pesanan/$1');
$routes->match(['post'], 'pesan/(:segment)', 'Pemesanan::pesan/$1');
$routes->match(['post'], 'upalamat/(:segment)', 'Pemesanan::alamatpesan/$1');
$routes->match(['post'], 'buktitf/(:segment)', 'Pemesanan::bukti/$1');
$routes->match(['post'], 'emailuser/(:segment)', 'Pemesanan::emailuser/$1');


/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
